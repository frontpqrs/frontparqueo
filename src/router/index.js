import Vue from 'vue'
import VueRouter from 'vue-router'
import Categoria from '../views/Categoria.vue'
import Login from '../views/Login.vue'
import Usuario from '../views/Usuario.vue'
import registro from '../views/Registro.vue'
import olvidoCOntrasenia from '../views/OlvidoContrasenia.vue'
import parqueadero from '../views/Parqueadero.vue'
import Vehiculo from '../views/Vehiculo.vue'
import CrearReserva from '../views/CrearReserva'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'login',
    component: Login

  },
  {
    path: '/categoria',
    name: 'categoria',
    component: Categoria

  },
  {
    path: '/usuario',
    name: 'usuario',
    component: Usuario

  },
  {
    path: '/registro',
    name: 'registro',
    component: registro

  },
  {
    path: '/parqueadero',
    name: 'parqueadero',
    component: parqueadero

  },
  {
    path: '/olvidoContrasenia',
    name: 'olvidoContrasenia',
    component: olvidoCOntrasenia

  },
  {
    path: '/vehiculo',
    name: 'Vehiculo',
    component: Vehiculo
  },
  {
    path: '/CrearReserva',
    name: 'CrearReserva',
    component: CrearReserva
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
